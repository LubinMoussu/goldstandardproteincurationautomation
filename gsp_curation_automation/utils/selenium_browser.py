import logging
import urllib
from time import sleep

import selenium.webdriver.support.ui as ui
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary
from selenium.webdriver.firefox.options import Options

logger = logging.getLogger(__name__)


class SeleniumBrowser:
    """
    """

    PUBSEED_BASE_URL = "http://pubseed.theseed.org/?"
    PUBSEED_LOGIN_PARAMETERS = {
        "page": "Login",
        "action": "perform_login",
        "login": "",
        "password": "",
    }

    def __init__(self, firefox_binary, geckodriver):
        """

        :param firefox_binary:
        :param geckodriver:
        """

        self.firefox_binary = firefox_binary
        self.geckodriver = geckodriver
        self.browser = None

    def init_headless_firefox_browser(self):
        """
        init headless firefox browser
        :return:
        """

        fp = webdriver.FirefoxProfile()

        options = Options()
        options.headless = True
        # options.headless = False

        browser = webdriver.Firefox(
            options=options,
            executable_path=self.geckodriver,
            firefox_binary=self.firefox_binary,
            firefox_profile=fp,
        )
        browser.set_page_load_timeout(30)
        logger.info("Headless Firefox Initialized")
        self.browser = browser

    def init_pubseed_login(self, pubseed_identifier, pubseed_password):
        """
        Login to Pubseed database
        :param pubseed_identifier:
        :param pubseed_password:
        :return:
        """

        values = self.PUBSEED_LOGIN_PARAMETERS
        values["login"] = pubseed_identifier
        values["password"] = pubseed_password

        # encode request
        data = urllib.parse.urlencode(values)
        # Create the request
        request = f"{self.PUBSEED_BASE_URL}{data}"

        try:
            self.browser.set_page_load_timeout(30)
            self.browser.get(request)

            sleep(3)
            logger.info(
                "Connected to PubSeed through user {}".format(
                    pubseed_identifier,
                ),
            )

        except urllib.error.HTTPError:
            pass
