import csv
import itertools
import operator
import os
import pickle
import urllib
from collections import Counter
from html.parser import HTMLParser
from itertools import groupby
from multiprocessing.dummy import Pool as ThreadPool
from operator import itemgetter
from os import startfile
from pprint import pprint
from random import randint
from random import sample
from random import seed
from random import SystemRandom
from re import findall
from re import match
from subprocess import Popen
from time import sleep
from time import time
from urllib.parse import urlencode
from urllib.request import Request
from urllib.request import urlopen
from webbrowser import open as webbrowser_open

import numpy as np
import pandas as pd
import scipy.stats as st
from buildref import get_reference_set
from evaluatecandidates import evaluate_candidate_cluster_list
from identifycandidates import identify_candidates_in_genomes
from Multi_project_scripts import get_csv_reader
from Multi_project_scripts import get_orgid_from_fig
from Multi_project_scripts import is_orthologuous
from Multi_project_scripts import merge_two_dicts
from pandas_ml import ConfusionMatrix

# from sklearn.naive_bayes import GaussianNB
# from sklearn.naive_bayes import ComplementNB
# from sklearn.model_selection import train_test_split
# from sklearn.metrics import confusion_matrix


def get_confusion_matrix(y_actu, y_pred):

    df_confusion = pd.crosstab(
        y_actu,
        y_pred,
        rownames=["Actual"],
        colnames=["Predicted"],
        margins=True,
    )
    df_conf_norm = df_confusion / df_confusion.sum(axis=1)
    print(df_confusion.print_stats())

    cm = ConfusionMatrix(y_actu, y_pred)
    cm.print_stats()

    return df_confusion, df_conf_norm


def compare_traning_testing_sets(fig_object_dict, all_candidate_cluster_annot):

    y_actu = []
    y_pred = []
    for fig in all_candidate_cluster_annot:
        if fig in fig_object_dict:
            y_pred.append(all_candidate_cluster_annot[fig])
            y_actu.append(fig_object_dict[fig].peg)
    return y_actu, y_pred


def getAccuracy(testSet, predictions):
    correct = 0
    for i in range(len(testSet)):
        if testSet[i][-1] == predictions[i]:
            correct += 1
    return (correct / float(len(testSet))) * 100.0
