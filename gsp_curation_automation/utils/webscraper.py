import random
import traceback
import urllib.request
from collections import Counter
from itertools import cycle
from time import sleep
from time import time

import numpy as np
import requests
import scipy.stats as st
from lxml.html import fromstring


def get_proxies():
    url = "https://free-proxy-list.net/"
    response = requests.get(url)
    parser = fromstring(response.text)
    proxies = set()
    for i in parser.xpath("//tbody/tr")[:1000]:
        if i.xpath('.//td[7][contains(text(),"yes")]') and i.xpath(
            './/td[5][contains(text(),"elite proxy")]',
        ):
            # Grabbing IP and corresponding PORT
            proxy = ":".join(
                [i.xpath(".//td[1]/text()")[0], i.xpath(".//td[2]/text()")[0]],
            )
            proxies.add(proxy)
    return proxies


def get_user_agent():

    url = "https://developers.whatismybrowser.com/useragents/explore/software_name/firefox/"
    response = requests.get(url)
    parser = fromstring(response.text)
    user_agent_list = set()

    for i in parser.xpath("//tbody/tr")[:100]:
        if (
            i.xpath('.//td[3][contains(text(),"Windows")]')
            and i.xpath('.//td[4][contains(text(), "Computer")]')
            and i.xpath('.//td[5][contains(text(), "Very common")]')
        ):

            user_agent = i.xpath(".//td[1]/a")[0]
            user_agent = str(user_agent.text_content())
            user_agent_list.add(user_agent)
    return user_agent_list

    # proxies = get_proxies()
    # proxy_pool = cycle(proxies)
    #
    # url = 'https://www.say-yess.com/'
    # for i in range(50):
    #     # Get a proxy from the pool
    #     proxy = next(proxy_pool)
    #     print("Request #%d" % i)
    #     try:
    #         response = requests.get(url, proxies={"http": proxy, "https": proxy}, timeout=30)
    #     except Exception as ex:
    #         print(ex)
    #         # Most free proxies will often get connection errors. You will have retry the entire request using another proxy to work.
    #         # We will just skip retries as its beyond the scope of this tutorial and we are only downloading a single url
    #         print("Skipping. Connnection error")

    # user_agent_list = get_user_agent()
    # print(user_agent_list)

    # url = 'https://httpbin.org/user-agent'
    # # Lets make 5 requests and see what user agents are used
    # for i in range(1, 6):
    #     # Pick a random user agent
    #     user_agent = random.choice(user_agent_list)
    #     # Set the headers
    #     headers = {'User-Agent': user_agent}
    #     # Make the request
    #     request = urllib.request.Request(url, headers={'User-Agent': user_agent})
    #     response = urllib.request.urlopen(request)
    #     html = response.read()
    #
    #     print("Request #%d\nUser-Agent Sent:%s\nUser Agent Recevied by HTTPBin:" % (i, user_agent))
    #     print(html)
    #     print("-------------------\n\n")


if __name__ == "__main__":

    proxies = [
        "140.227.81.53:3128",
        "79.104.25.218:8080",
        "85.236.234.163:8080",
        "36.67.199.185:6666",
        "45.221.73.46:8080",
        "83.166.229.24:3128",
        "128.199.66.99:8080",
        "41.76.91.202:8080",
        "128.199.102.176:8080",
        "103.69.219.81:8080",
        "190.60.103.178:8080",
        "217.219.133.45:8080",
        "68.183.113.39:8080",
        "101.108.252.153:8080",
        "186.248.80.3:80",
        "103.218.25.25:8080",
        "220.130.172.122:8080",
        "88.44.106.139:80",
        "183.89.59.57:8080",
        "189.3.160.4:3128",
        "188.68.186.115:8080",
        "103.218.27.33:8080",
        "181.67.123.134:8080",
        "82.209.49.194:8080",
        "111.90.179.42:8080",
        "103.224.48.113:8080",
        "103.206.253.78:8080",
        "89.178.234.177:8080",
        "105.112.83.163:8080",
        "103.36.121.5:8080",
        "178.128.222.152:8080",
        "87.255.13.217:8080",
        "182.253.177.19:8080",
        "50.224.238.78:8080",
    ]

    proxies = list(get_proxies())
    # proxy_pool = cycle(proxies)
    user_agent_list = list(get_user_agent())

    url_list = [
        "http://pubseed.theseed.org//seedviewer.cgi?page=Annotation&feature=fig|518637.5.peg.354",
    ]

    request_duration_list = []
    working_proxies = []

    for i in range(len(proxies)):
        start = time()

        # Get a proxy from the pool
        # proxy = random.choice(proxies)
        proxy = proxies[i]

        # Pick a random user agent and set the headers
        user_agent = random.choice(user_agent_list)
        headers = {"User-Agent": user_agent}

        url = random.choice(url_list)
        try:
            response = requests.get(
                url,
                headers={"User-Agent": user_agent},
                proxies={"http": proxy, "https": proxy},
                timeout=60,
            )
            if response:
                working_proxies.append(proxy)
        except Exception as ex:
            print(ex)

        end = time()
        request_duration = end - start
        request_duration_list.append(request_duration)
        sleep(1)

    print()
    print(working_proxies)
    print(len(working_proxies))

    # ic = st.t.interval(0.95, len(request_duration_list) - 1, loc=np.mean(request_duration_list), scale=st.sem(request_duration_list))
    # ic = ["%.1f" % ic[0], "%.1f" % ic[1]]
    # print(ic)
    # print()
    # print(min(request_duration_list), max(request_duration_list))
    # print(np.mean(request_duration_list))
    # print(np.std(request_duration_list))
