import base64
import logging
import os
import re
import shutil
import urllib
from csv import reader as csv_reader

import slugify

logger = logging.getLogger(__name__)


def get_csv_reader(file, delimiter="\t"):
    data = []
    with open(file) as fd:
        for line in csv_reader(fd, delimiter=delimiter):
            data.append(line)
    return data


def list2file(line_list: list, filename: str) -> None:
    try:
        with open(filename, "w") as f:
            f.writelines(line_list)
    except (FileNotFoundError, FileExistsError):
        pass


def file2list(filename: str) -> list:
    try:
        with open(filename) as f:
            line_list = f.readlines()
        return line_list
    except (FileNotFoundError, FileExistsError):
        pass


def makedirs_from_file(file: str) -> None:
    """
    makes all intermediate-level directories
    :param file:
    :return:
    """
    try:
        dirname = os.path.dirname(file)
        os.makedirs(dirname)
    except FileExistsError:
        pass


def rmtree_from_file(file: str) -> None:
    """
    delete an entire directory tree
    :param file:
    :return:
    """
    try:
        dirname = os.path.dirname(file)
        shutil.rmtree(dirname, ignore_errors=True)
    except OSError:
        pass


def list_files_from_dir(mypath: str) -> list:
    """
    list all content of a path and return only files
    :param mypath:
    :return:
    """
    onlyfiles = [
        f
        for f in os.listdir(mypath)
        if os.path.isfile(os.path.join(mypath, f))
    ]
    return onlyfiles


def url2filename(url: str):
    """

    :param url:
    :return:
    """
    url = url.encode("UTF-8")
    return base64.urlsafe_b64encode(url).decode("UTF-8")


def filename2url(f: str):
    """

    :param f:
    :return:
    """
    return base64.urlsafe_b64decode(f).decode("UTF-8")


def dict2filename(d: dict) -> str:
    """

    :param d:
    :return:
    """
    string = urllib.parse.urlencode(d)
    string = url2filename(url=string)
    return string


def filename2dict(f: str):
    """

    :param f:
    :return:
    """
    url = filename2url(f)
    decoded = urllib.parse.unquote(url)
    return decoded


def string2slug(s: str) -> str:
    """

    :param s:
    :return:
    """
    r = slugify.slugify(
        s,
        save_order=True,
        regex_pattern=r"[^-a-z0-9_=]+",
        replacements=[["&", "-"], ["=", "="], [",", "-"]],
    )
    return r


def dict2slug(d: dict) -> str:
    """

    :param d:
    :return:
    """
    url = urllib.parse.urlencode(d)
    decoded = string2slug(url)
    return decoded
