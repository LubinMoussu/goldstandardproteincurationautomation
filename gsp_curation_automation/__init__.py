import logging
import os
from logging.config import fileConfig

from gsp_curation_automation.settings import project_config

TESTING = False
LOGGER_CONFIG_FILE = os.path.join(
    os.path.dirname(os.path.realpath(__file__)), "logging.ini",
)


def init_logger(logger_config_file=LOGGER_CONFIG_FILE):
    """

    :param logger_config_file:
    :return:
    """
    logging.propagate = False
    logging.getLogger().setLevel(logging.ERROR)
    logging.getLogger("gsp_cutation_automation").disabled = False

    logger = logging.getLogger(__name__)
    logger.addHandler(logging.NullHandler())
    logging.config.fileConfig(
        logger_config_file, disable_existing_loggers=False,
    )

    # HTTP connection
    logging.getLogger("urllib3").setLevel(logging.WARNING)

    # Requests
    logging.getLogger("requests").setLevel(logging.WARNING)

    # Decorators log_method
    logging.getLogger("gsp_cutation_automation.utils.decorators").setLevel(
        logging.DEBUG,
    )


if os.path.isfile(LOGGER_CONFIG_FILE):
    init_logger()
