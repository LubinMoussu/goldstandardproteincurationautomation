class Article:
    def __init__(self, pmid: str, name="", doi=""):
        """

        :param pmid: pubmed identifier
        :param name: name of the article
        :param doi: doi
        """
        self.pmid = pmid
        self.name = name
        self.doi = doi

    def __str__(self):
        return str(self.pmid)
