# Standard library imports
import logging
from re import findall

import requests as r

from gsp_curation_automation.scrapers.seed_check_search import (
    PubSeedCheckSearch,
)
from gsp_curation_automation.scrapers.seed_scraper import SeedScraper

# Local application imports

try:
    from BeautifulSoup import BeautifulSoup
except ImportError:
    from bs4 import BeautifulSoup

DEFAULT_SUBDIR_GSPScraper = "gold_standard_protein"
logger = logging.getLogger(__name__)


class Protein:
    PUBSEED_FIG_URL = "http://pubseed.theseed.org/seedviewer.cgi?"
    FIG_REGEX = r"fig\|\d+\.\d+\.peg.\d+"
    _fig_intrinsic_regex = r"fig\|(\d+).(\d+).peg.(\d+)"
    EC_NUMBER_PATTERN_REGEX = r"EC (\d+.\d+.[\d-]+.[\d-]+)"

    PUBSEED_BLAST_PARAMETERS = {
        "seq_type": "aa",
        "filter": "F",
        "evalue": 1e-05,
        "wsize": "0",
        "fasta": "",
        "organism": "511145.12",
        "act": "BLAST",
    }

    def __init__(self, protein_id):
        self.protein_id = protein_id
        self.pubseed_identifiers = []
        self.accession_code_list = [protein_id]
        self.protein_sequence = ""
        self.genes = []
        self.taxonomic_identifier = ""
        self.function = ""
        self.pubseed_function = ""
        self.molecular_function_ontology = []

    @staticmethod
    def get_genome_id_from_fig(fig):
        try:
            genome_id = findall(r"(\d+\.\d+).peg", fig)[0]
        except IndexError:
            genome_id = ""
        return genome_id

    def get_fig_intrinsic_information(self, fig):
        try:
            genome_id, genome_version, peg = findall(
                self._fig_intrinsic_regex, fig,
            )[0]
        except IndexError:
            genome_id = genome_version = peg = ""
        return genome_id, genome_version, peg

    @staticmethod
    def get_seed_genomes_from_taxonomic_identifier(
        taxonomic_identifier, all_pubseed_genomes,
    ):
        """

        :param taxonomic_identifier:
        :param all_pubseed_genomes:
        :return:
        """

        taxonomic_identifier = taxonomic_identifier.split(".")[0]
        matching_genomes = [
            elt
            for elt in all_pubseed_genomes
            if taxonomic_identifier == elt.split(".")[0]
        ]
        return matching_genomes

    def is_crossed_reference(self, fig_information):

        try:
            if fig_information["identities"][1] == "100":
                return True
            return False
        except KeyError or IndexError:
            logging.error("")
            return False

    def get_crossed_reference_from_uniprot_protein(
        self, uniprot_entry, genome_id,
    ):
        """
        Get sequences, producing significant alignments from the uniprot_entry
        :return:
        """

        (
            query_fig,
            fig_similarity_information,
        ) = SeedScraper().get_protein_significant_similarities(
            query_fig=uniprot_entry["annotation_information"],
            fasta_seq=uniprot_entry["seq"],
            genome_id=genome_id,
        )

        for fig in fig_similarity_information:
            if self.is_crossed_reference(fig_similarity_information[fig]):
                yield fig

    def get_pubseed_entries_from_uniprot_protein_sequences(
        self, uniprot_entry, all_pubseed_genomes,
    ):
        if uniprot_entry and "taxonomic_identifier" in uniprot_entry:
            taxonomic_identifier = uniprot_entry["taxonomic_identifier"]
            matching_genomes = self.get_seed_genomes_from_taxonomic_identifier(
                taxonomic_identifier, all_pubseed_genomes,
            )
            for genome_id in matching_genomes:
                candidate_fig_list = self.get_crossed_reference_from_uniprot_protein(
                    uniprot_entry, genome_id,
                )
                yield from candidate_fig_list

    @staticmethod
    def get_uniprot_names_taxonomy_fasta(uniprot_id):
        """

        :param uniprot_id:
        :return:
        """

        url = f"https://www.uniprot.org/uniprot/{uniprot_id}.fasta"
        query = url

        # Make the query
        res = r.get(query)

        if res.status_code == 404:
            return {}
        try:
            if res.status_code == 200 and res.text:
                response = res.text.split("\n")
                seq = "".join(response[1:])

                if response:
                    annotation_information = response[0]
                    organism_info = findall(
                        r"OS=(.+) OX=(\d*)", annotation_information,
                    )[0]
                    if organism_info:
                        organism_name, taxonomic_identifier = organism_info

                        if "strain" in organism_name:
                            genus, strains = findall(
                                r"(.+) \(strain (.+)\)", organism_name,
                            )[0]
                            strains = strains.split(" / ")
                            genome_names = []
                            for strain in strains:
                                genome_name = f"{genus} {strain}"
                                genome_names.append(genome_name)
                        else:
                            genome_names = [organism_name]

                    genes = findall(r"GN=(\w+)", annotation_information)

                    uniprot_entry = {
                        "annotation_information": uniprot_id,
                        "seq": seq,
                        "genome_names": genome_names,
                        "taxonomic_identifier": taxonomic_identifier,
                        "genes": genes,
                    }
                    return uniprot_entry
        except (KeyboardInterrupt, IndexError) as exception:
            logging.error(
                "get_uniprot_names_taxonomy_fasta method: {} exception".format(
                    exception,
                ),
            )

    def get_latest_seed_peg_given_genome_version(self, fig_list):
        """
        Genomes are subject to versions. Amongst a list of PEGs that correspond to the same PEG but with different genome version,
        select the PEG with the latest genome
        :param fig_list:
        :return:
        """

        fig_dict = {}
        for fig in fig_list:
            (
                genome_id,
                genome_version,
                peg,
            ) = self.get_fig_intrinsic_information(fig)
            try:
                fig_dict[genome_id].append(int(genome_version))
            except (IndexError, KeyError):
                fig_dict[genome_id] = [int(genome_version)]

        fig_dict = {k: sorted(v)[-1] for k, v in fig_dict.items() if v}

        updated_pubseed_identifiers = [
            fig
            for k, v in fig_dict.items()
            for fig in fig_list
            if f"fig|{k}.{v}.peg." in fig
        ]
        return updated_pubseed_identifiers

    def get_pubseed_entries_for_protein(self, all_pubseed_genomes):
        """
        Aim: Determine if the protein is present in PubSeed. If so, save all PubSeed matching entries into self.pubseed_identifiers
        """

        pubseed_protein_identifiers = [
            elt
            for elt in self.accession_code_list
            if findall(self.FIG_REGEX, elt)
        ]
        logging.debug(
            "pubseed_protein_identifiers:", pubseed_protein_identifiers,
        )
        if pubseed_protein_identifiers:
            self.pubseed_identifiers.extend(pubseed_protein_identifiers)

        else:
            # For each protein_name of the gsp, check if this protein_name exists in PubSeed
            pubseed_check_search = PubSeedCheckSearch(protein_id="")
            for protein_id in self.accession_code_list:
                pubseed_matches_features = pubseed_check_search(protein_id)

                if not pubseed_matches_features:
                    # If entry_id of databases are not found in PubSeed, the protein sequence is retrieved from databases and blast against genomes of PubSeed
                    uniprot_entry = self.get_uniprot_names_taxonomy_fasta(
                        protein_id,
                    )
                    if uniprot_entry and uniprot_entry["taxonomic_identifier"]:
                        # If there is a identified reference genome, find PubSeed candidates
                        pubseed_matches_features = self.get_pubseed_entries_from_uniprot_protein_sequences(
                            uniprot_entry, all_pubseed_genomes,
                        )

                if pubseed_matches_features:
                    # Check if genome_ID is within expected genome_IDs in genome_set
                    pubseed_matches_features = list(
                        {
                            fig
                            for fig in pubseed_matches_features
                            if self.get_genome_id_from_fig(fig)
                            in all_pubseed_genomes
                        },
                    )

                    if pubseed_matches_features:
                        # add of PubSeed_entries to the gsp.pubseed_identifiers
                        updated_pubseed_identifiers = list(
                            set(self.pubseed_identifiers)
                            | set(pubseed_matches_features),
                        )
                        self.pubseed_identifiers = list(
                            updated_pubseed_identifiers,
                        )

        if self.pubseed_identifiers:
            # Reduce the number of pubseed identifiers: different versions of pubseed genome exist so we take only the latess one
            self.pubseed_identifiers = self.get_latest_seed_peg_given_genome_version(
                self.pubseed_identifiers,
            )

    def set_protein_obj_pubseed_fasta_information(self):
        """
        Retrieve self.protein_sequence and self.pubseed_function from PubSeed
        :return:
        """

        fig = self.pubseed_identifiers[0]
        feature_info = SeedScraper().query_fasta_sequences(fig)
        self.protein_sequence = feature_info["Seq"]
        self.pubseed_function = feature_info["Prot_name"]
        return self
