# Standard library imports
import logging

from gsp_curation_automation.protein.protein import Protein

# Local application imports

DEFAULT_SUBDIR_GSPScraper = "seed_peg"
logger = logging.getLogger(__name__)


class SeedPEG(Protein):
    def __init__(self, entry_id):
        super().__init__(entry_id)
