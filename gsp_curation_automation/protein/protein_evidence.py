import logging

try:
    from BeautifulSoup import BeautifulSoup
except ImportError:
    from bs4 import BeautifulSoup

DEFAULT_SUBDIR_GSPScraper = "gold_standard_protein"
logger = logging.getLogger(__name__)


class ProteinEvidence:
    """
    Provide information about the protein from scientific articles
    """

    def __init__(
        self,
        protein_id: str,
        protein_id_list: list,
        genome_id: str,
        evidence,
        pubmed_id_list: list,
    ):
        self.protein_id = protein_id
        self.protein_ids = protein_id_list
        self.genome_id = genome_id
        self.evidence = evidence
        self.pubmed_ids = pubmed_id_list
