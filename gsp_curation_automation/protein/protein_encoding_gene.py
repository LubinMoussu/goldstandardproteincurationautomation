class ProteinEncodingGene:
    _database_priorities = ["SwissProt", "EcoCyc", "MetaCyc", "BRENDA"]

    def __init__(
        self, id, synonyms, function_list, organism_list=None, sequence=None,
    ):
        self.id = id
        self.synonyms = synonyms
        self.function_list = function_list
        self.organism_list = organism_list
        self.sequence = sequence

        if id is None:
            self.set_id()

    def set_id(self) -> None:
        """
        Iterate through db_names to find most suitable protein_id
        prefer SwissProt, EcoCyc or MetaCyc protein_id which turns out to be index-1 generally
        :return:
        """
        my_id = None
        for db in self._database_priorities:
            if my_id is None:
                for function in self.function_list.values():
                    if function.db_id == db:
                        my_id = function.protein_encoding_gene_id[-1]
        self.id = my_id

    def set_synonyms(self) -> None:
        """
        Group all names saved into function object
        :return:
        """
        synonyms = list(
            {
                elt
                for function in self.function_list.values()
                for elt in function.protein_encoding_gene_id
            },
        )
        case_insensible_synonyms = []
        marker = set()

        for l in synonyms:
            ll = l.lower()
            if ll not in marker:  # test presence
                marker.add(ll)
                case_insensible_synonyms.append(l)
        self.synonyms = case_insensible_synonyms
