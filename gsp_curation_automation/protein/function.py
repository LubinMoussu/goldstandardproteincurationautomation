class Function:
    def __init__(
        self,
        functional_role,
        article_list=None,
        protein_encoding_gene_id=None,
        db_id=None,
        genome_name=None,
        catalytic_activity=None,
        cofactors=None,
        substrates=None,
    ):
        if cofactors is None:
            cofactors = []
        if substrates is None:
            substrates = []
        if article_list is None:
            article_list = []

        self.functional_role = functional_role
        self.article_list = article_list
        self.protein_encoding_gene_id = protein_encoding_gene_id
        self.db_id = db_id
        self.genome_name = genome_name
        self.catalytic_activity = catalytic_activity
        self.cofactors = cofactors
        self.substrates = substrates
