import logging
import urllib
from re import findall
from urllib.parse import urlencode

from gsp_curation_automation.protein.protein import Protein
from gsp_curation_automation.scrapers.seed_scraper import SeedScraper
from gsp_curation_automation.utils.similaritymeasures import jaccard_similarity

try:
    from BeautifulSoup import BeautifulSoup
except ImportError:
    from bs4 import BeautifulSoup

DEFAULT_SUBDIR_GSPScraper = "gold_standard_protein"
logger = logging.getLogger(__name__)


class GoldStandardProtein(Protein):
    JACCARD_THRESHOLD = 0.6

    def __init__(
        self,
        accession_code_list,
        database_sources,
        entry_id,
        functional_roles,
        genome_names,
        pubseed_identifiers,
        dbs_associated_pubmed_identifiers,
        genes,
    ):

        super().__init__(entry_id)
        self.accession_code = accession_code_list
        self.database_sources = database_sources
        self.entry_id = entry_id
        self.functional_roles = functional_roles
        self.genome_names = genome_names
        self.pubseed_identifiers = pubseed_identifiers
        self.dbs_associated_pubmed_identifiers = (
            dbs_associated_pubmed_identifiers
        )
        self.associated_pubmed_identifiers = list(
            {
                pubmed_id
                for site in dbs_associated_pubmed_identifiers
                for pubmed_id in dbs_associated_pubmed_identifiers[site]
            },
        )
        self.genes = genes
        self.consistency = False
        self.features_to_curate = []

    def add_pubmed_identifier_to_fig_selenium(
        self, fig, pubmed_identifier, selenium_browser,
    ):

        if pubmed_identifier.isdigit() and isinstance(pubmed_identifier, str):

            values = {
                "page": "EditPubMedIdsDSLits",
                "feature": fig,
                "PMID": pubmed_identifier,
                "SaveNew": "Save to Dlits",
                "getPubmeds": "-1",
                "user": "moussulubin",
            }

            # Encode values in url code
            fig = fig.encode(encoding="UTF-8", errors="strict")

            data = urllib.parse.urlencode(values)
            # Create the request
            request = f"{self.PUBSEED_FIG_URL}{data}"

            try:
                selenium_browser.set_page_load_timeout(30)
                selenium_browser.get(request)

                # TODO test the status_code of the get
                logger.info(
                    "{} pubmed identifier added to {} fig".format(
                        pubmed_identifier, fig,
                    ),
                )
                return selenium_browser
            except Exception as ex:
                print("add_pubmed_identifier_to_fig_selenium:", ex)

    def curate_literature_of_pubseed_fig(
        self,
        fig,
        current_pubmed_identifiers,
        entry_pubmed_identifiers,
        browser,
    ):

        # if the PubSeed fig is not associated with any pubmed_id which however should be associated, add the pubmed_id to PubSeed_ID
        if not current_pubmed_identifiers:
            # Define pubmed_id to add to the fig
            pubmed_ids_to_add = [
                pubmed_ID
                for pubmed_ID in entry_pubmed_identifiers
                if pubmed_ID not in current_pubmed_identifiers
            ]

            for pubmed_id in pubmed_ids_to_add:
                self.add_pubmed_identifier_to_fig_selenium(
                    fig, pubmed_id, browser,
                )
                break

    @staticmethod
    def update_functional_role_consistency(
        current_role_ec,
        reference_role_ec,
        current_role,
        reference_role,
        jaccard_threshold,
        ec_number_sufficiency,
    ):
        """
        Compare two functional role together
        :param current_role_ec:
        :param reference_role_ec:
        :param current_role:
        :param reference_role:
        :param jaccard_threshold:
        :param ec_number_sufficiency:
        :return:
        """

        if current_role_ec and reference_role_ec:
            reference_role_ec = (
                reference_role_ec[0] if reference_role_ec else ""
            )
            if ec_number_sufficiency and reference_role_ec == current_role_ec:
                # If both EC numbers are equal
                return True
            else:
                jaccard_score = jaccard_similarity(
                    current_role, reference_role,
                )
                if jaccard_score >= jaccard_threshold:
                    return True
        return False

    def get_uniprot_potential_functional_roles(self, functional_roles):

        try:
            ec_number = findall(
                self.EC_NUMBER_PATTERN_REGEX, functional_roles[0],
            )[0]
        except IndexError:
            ec_number = ""

        print(functional_roles, ec_number)
        table_functional_roles = functional_roles.split("; ")
        uniprot_potential_role = (
            "{} ({})".format(table_functional_roles[0], ec_number)
            if ec_number
            else table_functional_roles
        )
        return uniprot_potential_role

    def estimate_functional_role_consistency(
        self, fig_information, jaccard_threshold, ec_number_sufficiency,
    ):
        """
        Aim: check that the functional role fits ontology systems role2
        Process: (1) take role1, role2
                 (2) if ec_number_sufficiency, just check that EC number is the same
                 (3) if not ec_number_sufficiency, calculate jaccard_similarity between both roles
                 (4) return bolean of role1 role2 consistency
        :param role1:
        :param role2:
        :return:
        """

        ec_number_pattern = self.EC_NUMBER_PATTERN_REGEX
        current_role = fig_information["Function"]
        current_role_ec = findall(ec_number_pattern, current_role)
        current_role_ec = current_role_ec[0] if current_role_ec else ""
        reference_roles = self.functional_roles

        consistency = False
        for site in reference_roles:
            if site == "SwissProt":
                list_reference_roles = self.get_uniprot_potential_functional_roles(
                    reference_roles[site],
                )
            else:
                list_reference_roles = list(reference_roles[site])

            for reference_role in list_reference_roles:
                reference_role_ec = findall(ec_number_pattern, reference_role)
                consistency = self.update_functional_role_consistency(
                    current_role_ec,
                    reference_role_ec,
                    current_role,
                    reference_role,
                    jaccard_threshold,
                    ec_number_sufficiency,
                )
                if consistency:
                    return consistency
        return consistency

    def update_literature_pubseed_feature(
        self, selenium_browser, ec_number_sufficiency,
    ):
        """
        For all fig assigned to a protein_entity, assign the pubmed_id and check consistency of the annotation
        :param selenium_browser:
        :param ec_number_sufficiency:
        :return:
        """

        features_to_curate = {}
        if (
            self.entry_id
            and self.pubseed_identifiers
            and self.associated_pubmed_identifiers
        ):
            # if the self.entry_id has an entry in PubSeed and an evidence article, check if the article is assigned to PubSeed entry
            for fig in self.pubseed_identifiers:
                all_fig_information = SeedScraper().get_pubseed_single_fig_information(
                    fig,
                )

                # Curate literature of the fig
                self.curate_literature_of_pubseed_fig(
                    fig,
                    all_fig_information["Ev"],
                    self.associated_pubmed_identifiers,
                    selenium_browser,
                )

                # check if the current annotation is consistent with reference annotations. If not, trigger manual curation
                consistency = self.estimate_functional_role_consistency(
                    fig_information=all_fig_information,
                    jaccard_threshold=self.JACCARD_THRESHOLD,
                    ec_number_sufficiency=ec_number_sufficiency,
                )

                if not consistency:
                    try:
                        features_to_curate[self.entry_id].append(fig)
                    except IndexError:
                        features_to_curate[self.entry_id] = [fig]
        return features_to_curate

    def gsp_object_pipeline(
        self, selenium_browser, ec_number_sufficiency, all_pubseed_genomes,
    ):
        """
        1. if pubmed_ID not present, break pipeline
        2. if gsp not present in PubSeed, break pipeline
        3. Add to pubmed_ID to the PubSeed_GSP
        4. if bad functional_annotation consistency, change functional_annotation
        :param selenium_browser:
        :param ec_number_sufficiency:
        :param all_pubseed_genomes:
        :return:
        """

        logger.info(
            "{} Pubmed identifiers found for the GSP {}".format(
                self.associated_pubmed_identifiers, self.entry_id,
            ),
        )
        consistency = False
        features_to_curate = []
        if self.associated_pubmed_identifiers:
            # at least 1 pubmed_ID exists

            # Find PubSeed fig associated with this GSP
            self.get_pubseed_entries_for_protein(all_pubseed_genomes)
            logger.info(
                "{} PubSeed proteins found referring to GSP {}".format(
                    self.pubseed_identifiers, self.entry_id,
                ),
            )
            if self.pubseed_identifiers:
                # Add to pubmed_ID to the PubSeed_GSP
                (
                    consistency,
                    features_to_curate,
                ) = self.update_literature_pubseed_feature(
                    selenium_browser.browser, ec_number_sufficiency,
                )
        self.consistency = consistency
        self.features_to_curate = features_to_curate
