import urllib
from time import sleep

import ratelimiter

try:
    from BeautifulSoup import BeautifulSoup
except ImportError:
    from bs4 import BeautifulSoup

from urllib.parse import urlparse, parse_qs, urlencode
import bs4

from gsp_curation_automation.utils.decorators import log_method


class SeedScraper:
    ORG_SEEDVIEWER_CGI_ = "http://pubseed.theseed.org//seedviewer.cgi?"
    _peg_retreive_url = "http://pubseed.theseed.org//protein.cgi?"
    _relevant_descripters = [
        "End",
        "Ev",
        "Fid",
        "Function",
        "Gap",
        "Size",
        "Start",
        "strand",
    ]

    def __init__(self):

        self.user = "moussulubin"

    @log_method
    @ratelimiter.RateLimiter(max_calls=100, period=3600)  # 100 hits per hour
    @ratelimiter.RateLimiter(max_calls=150, period=60)  # 150 hits per min
    @ratelimiter.RateLimiter(max_calls=5, period=1)  # 5 hits per sec
    def get_peg_information(self, fig):

        values = {"prot": fig, "user": "moussulubin"}
        # Encode values in url code
        data = urlencode(values)
        # Create the request
        request = f"{self._peg_retreive_url}&{data}"
        opener = urllib.request.build_opener()
        try:
            response = opener.open(request, timeout=120)
            sleep(1)
            return response
        except Exception as ex:
            print("get_peg_information", fig, ex)

    def get_query_fasta_sequences(self, feature):
        """ Query fasta sequence of feature in PubSEED
        """

        values = {
            "page": "ShowSeqs",
            "feature": feature,
            "FLANKING": "500",
            "Sequence": "Protein Sequence",
            "Download": "Download Sequences",
        }
        for key in values:
            values[key] = str(values[key])

        # Encode values in url code
        data = urlencode(values)
        # Create the request
        request = self.ORG_SEEDVIEWER_CGI_ + data
        opener = urllib.request.build_opener()

        while True:
            try:
                response = opener.open(request, timeout=60)
                sleep(1)
                return response
            except Exception as ex:
                print("query_fasta_sequences", feature, ex)

    def query_fasta_sequences(self, feature):

        response = self.get_query_fasta_sequences(feature)
        response = [
            elt.decode("utf-8").rstrip("\n")
            for elt in response
            if elt.decode("utf-8").rstrip("\n")
        ]
        seq = "".join(response[1:])
        full_name = response[0][1:]

        tot = full_name.split(" [")
        tot = tot[0] if len(tot) == 2 else " [".join(tot[:2])

        prot_name = tot.lstrip(f"{feature} ").split("##")[0].rstrip(" ")
        feature_info = {"Fig": feature, "Prot_name": prot_name, "Seq": seq}
        return feature_info

    def get_pubseed_fig_information(self, fig):

        page = self.get_peg_information(fig)
        soup = BeautifulSoup(page, "html.parser")

        all_fig_information = {}
        for link in soup.body.find_all(
            "span", attrs={"id": "chromosome_context_block_content"},
        ):
            for table in link.find_all("table"):

                trs = list(table.find_all("tr"))
                header = [elt for elt in list(trs[0]) if elt.name == "th"]
                header = [
                    elt.string if len(elt.contents) == 1 else elt.contents[0]
                    for elt in header
                ]

                for tr in trs[1:]:
                    feature_info = [
                        elt for elt in list(tr) if elt.name == "td"
                    ]
                    feature_info = [
                        elt.contents
                        if elt.find_all("a", href=True)
                        else elt.string
                        for elt in feature_info
                    ]

                    fig_descripters = {
                        var: feature_info[header.index(var)] for var in header
                    }
                    fig_descripters["Fid"] = parse_qs(
                        urlparse(fig_descripters["Fid"][0]["href"]).query,
                    )["feature"][0]

                    if fig_descripters["Fid"]:
                        fig_descripters["strand"] = fig_descripters.pop("\xa0")
                        # fig_descripters['Ev']
                        if (
                            fig_descripters["Ev"]
                            and type(fig_descripters["Ev"][0])
                            == bs4.element.Tag
                        ):
                            fig_descripters["Ev"] = [
                                str(a.string)
                                for a in fig_descripters["Ev"][0].find_all(
                                    "a", href=True,
                                )
                            ]
                        else:
                            fig_descripters["Ev"] = []

                        # fig_descripters['Function']
                        if fig_descripters["Function"]:
                            if type(fig_descripters["Function"]) == list:
                                fig_descripters["Function"] = "".join(
                                    [
                                        elt.string
                                        for elt in fig_descripters["Function"]
                                    ],
                                )
                            else:
                                fig_descripters["Function"] = str(
                                    fig_descripters["Function"],
                                )
                        else:
                            fig_descripters["Function"] = ""

                        # clean
                        fig_descripters = {
                            key: fig_descripters[key]
                            if type(fig_descripters[key]) == list
                            else str(fig_descripters[key])
                            for key in self._relevant_descripters
                        }

                        if fig_descripters["Fid"] not in all_fig_information:
                            all_fig_information[
                                fig_descripters["Fid"]
                            ] = fig_descripters
        return all_fig_information

    def get_pubseed_single_fig_information(self, fig):
        """
        Aim: From pubseed information from a whole cluster, get only the pubseed information for the fig
        :param fig:
        :return:
        """
        return self.get_pubseed_fig_information(fig)[fig]
