# Standard library imports
import logging
import urllib
from re import findall
from time import sleep
from urllib.request import Request

# Local application imports

try:
    from BeautifulSoup import BeautifulSoup
except ImportError:
    from bs4 import BeautifulSoup

DEFAULT_SUBDIR_GSPScraper = "gold_standard_protein"
logger = logging.getLogger(__name__)


class PubSeedCheckSearch:
    _pubseed_check_search_url = "http://pubseed.theseed.org//seedviewer.cgi?pattern={0}&page=Find&act=check_search"
    _fig_regex = r"fig\|\d+\.\d+\.peg.\d+"

    def __init__(self, protein_id):
        """

        :param protein_id:
        """
        self.protein_id = protein_id

    def query_protein_id_to_pubseed_db(self, protein_id):
        """
        Query the protein_id to PubSeed to check the presence/absence of the protein_id
        :param protein_id:
        :return:
        """

        url = self._pubseed_check_search_url.format(protein_id)
        # Check if the protein_id coming from different databases is present in PubSeed
        request = url
        opener = urllib.request.build_opener()
        try:
            response = opener.open(request, timeout=30)
            sleep(1)
            return response
        except urllib.error.URLError:
            return None

    def get_pubseed_matches_features_from_protein_id(self, protein_id):
        """

        :param protein_id:
        :return:
        """

        page = self.query_protein_id_to_pubseed_db(protein_id)
        matches_features = []
        if page:
            soup = BeautifulSoup(page, "html.parser")

            for link in soup.find_all("input", attrs={"id": "table_data_0"}):
                figs = findall(self._fig_regex, str(link))
                for fig in figs:
                    matches_features.append(fig)
            matches_features = list(set(matches_features))
        return matches_features

    def __call__(self, protein_id):
        return self.get_pubseed_matches_features_from_protein_id(protein_id)
