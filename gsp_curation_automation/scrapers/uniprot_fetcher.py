import json
import logging

import ratelimiter
import requests

from gsp_curation_automation.utils import miscellaneous

logger = logging.getLogger(__name__)


class UniprotFetcher:
    protein_query_url = "https://www.ebi.ac.uk/proteins/api/proteins"
    response_content_type = "application/json"

    key2filter = ["accession", "sequence", "organism", "gene"]

    def __init__(self, uniprot_id: str):

        self.uniprot_id = uniprot_id

    def make_retrieve_protein_entry_url(self) -> str:
        """

        :return:
        """
        return f"{self.protein_query_url}/{self.uniprot_id}"

    @ratelimiter.RateLimiter(max_calls=100, period=3600)  # 100 hits per hour
    @ratelimiter.RateLimiter(max_calls=150, period=60)  # 150 hits per min
    @ratelimiter.RateLimiter(max_calls=5, period=1)  # 5 hits per sec
    def get_response_body(self, request_url) -> dict:
        r = requests.get(
            request_url, headers={"Accept": self.response_content_type},
        )
        if not r.ok:
            r.raise_for_status()

        # Get r.text and deserialize it
        json_string = r.text
        response_body = json.loads(json_string)
        return response_body

    @staticmethod
    def save_response_body(response_body, file_out) -> None:
        """
        dump response body into file
        :param file_out:
        :param response_body:
        :return:
        """
        # makedirs
        miscellaneous.makedirs_from_file(file_out)

        with open(file_out, "w") as f:
            json.dump(response_body, f)

    @staticmethod
    def load_response_body(file_in) -> dict:
        """

        :param file_in:
        :return:
        """
        try:
            with open(file_in) as f:
                response_body = json.load(f)
                return response_body
        except (FileNotFoundError, ValueError):
            logger.warning("Could not retrieve annotation result set")

    def retrieve_protein_entry(self):
        """

        :return:
        """
        request_url = self.make_retrieve_protein_entry_url()
        response_body = self.get_response_body(request_url=request_url)
        return response_body

    def filter_protein_entry(self, response_body):
        """

        :param response_body:
        :return:
        """
        filtered_response_body = {
            k: v for k, v in response_body.items() if k in self.key2filter
        }
        return filtered_response_body

    # @staticmethod
    # def get_uniprot_names_taxonomy_fasta(uniprot_id):
    #     """
    #
    #     :param uniprot_id:
    #     :return:
    #     """
    #
    #     url = f"https://www.uniprot.org/uniprot/{uniprot_id}.fasta"
    #     query = url
    #
    #     # Make the query
    #     res = requests.get(query)
    #
    #     if res.status_code == 404:
    #         return {}
    #     try:
    #         if res.status_code == 200 and res.text:
    #             response = res.text.split("\n")
    #             seq = "".join(response[1:])
    #
    #             if response:
    #                 annotation_information = response[0]
    #                 organism_info = findall(
    #                     r"OS=(.+) OX=(\d*)", annotation_information,
    #                 )[0]
    #                 if organism_info:
    #                     organism_name, taxonomic_identifier = organism_info
    #
    #                     if "strain" in organism_name:
    #                         genus, strains = findall(
    #                             r"(.+) \(strain (.+)\)", organism_name,
    #                         )[0]
    #                         strains = strains.split(" / ")
    #                         genome_names = []
    #                         for strain in strains:
    #                             genome_name = f"{genus} {strain}"
    #                             genome_names.append(genome_name)
    #                     else:
    #                         genome_names = [organism_name]
    #
    #                 genes = findall(r"GN=(\w+)", annotation_information)
    #
    #                 uniprot_entry = {
    #                     "annotation_information": uniprot_id,
    #                     "seq": seq,
    #                     "genome_names": genome_names,
    #                     "taxonomic_identifier": taxonomic_identifier,
    #                     "genes": genes,
    #                 }
    #                 return uniprot_entry
    #     except (KeyboardInterrupt, IndexError) as exception:
    #         logging.error(
    #             "get_uniprot_names_taxonomy_fasta method: {} exception".format(
    #                 exception,
    #             ),
    #         )
