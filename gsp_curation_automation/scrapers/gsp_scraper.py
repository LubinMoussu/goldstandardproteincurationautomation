# Standard library imports
import itertools
import json
import logging
import urllib
from multiprocessing.dummy import Pool as ThreadPool
from time import sleep
from urllib.parse import urlencode
from urllib.request import Request

import bs4
import pandas as pd
import ratelimiter
import requests
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary

from gsp_curation_automation.protein.article import Article
from gsp_curation_automation.protein.database import Database
from gsp_curation_automation.protein.function import Function
from gsp_curation_automation.protein.gsp import GoldStandardProtein
from gsp_curation_automation.protein.organism import Organism
from gsp_curation_automation.protein.protein import Protein
from gsp_curation_automation.protein.protein_encoding_gene import (
    ProteinEncodingGene,
)
from gsp_curation_automation.settings import project_config
from gsp_curation_automation.utils.decorators import log_method
from gsp_curation_automation.utils.miscellaneous import get_csv_reader
from gsp_curation_automation.utils.selenium_browser import SeleniumBrowser

# Local application imports

DEFAULT_SUBDIR_GSPScraper = "gold_standard_protein"
logger = logging.getLogger(__name__)


class GoldStandardProteinScraper:
    _paperblast_search_params = {"query": "", "Search": "Search"}
    _paperblast_url = "http://papers.genomics.lbl.gov/cgi-bin/litSearch.cgi?"
    _targeted_sites = ["SwissProt", "EcoCyc", "MetaCyc", "BRENDA"]

    _fig_regex = r"fig\|\d+\.\d+\.peg.\d+"
    _ec_number_pattern_regex = r"EC (\d+.\d+.[\d-]+.[\d-]+)"

    response_content_type = "test/javescript"

    _pubseed_blast_parameters = {
        "seq_type": "aa",
        "filter": "F",
        "evalue": 1e-05,
        "wsize": "0",
        "fasta": "",
        "organism": "511145.12",
        "act": "BLAST",
    }

    def __init__(self, workers, ec_number_sufficiency, depth):

        self.output_folder = project_config["output_folder"]
        self.workers = workers
        self.genome_set = []
        self.gold_standard_proteins = {}
        self.submitted_protein_identifiers = {}
        self.file_dictionary = {
            "seed_genomes": project_config["seed_genomes"],
        }
        self.ec_number_sufficiency = ec_number_sufficiency
        self.depth = depth
        self.selenium_browser = None

    def get_pubseed_genome_set(self):

        self.genome_set = get_csv_reader(
            self.file_dictionary["PubSeed_genome_set"], delimiter=";",
        )[0]

    def encode_paperblast_query(self, seq: str) -> str:
        """

        :param seq:
        :return:
        """
        values = self._paperblast_search_params.copy()
        values["query"] = seq
        values = {k: str(v) for k, v in values.items()}
        # Encode values in url code
        data = urlencode(values)
        # Create the querystring
        return f"{self._paperblast_url}&{data}"

    @ratelimiter.RateLimiter(max_calls=100, period=3600)  # 100 hits per hour
    @ratelimiter.RateLimiter(max_calls=150, period=60)  # 150 hits per min
    @ratelimiter.RateLimiter(max_calls=5, period=1)  # 5 hits per sec
    def get_response_body(self, querystring):
        """

        :param querystring:
        :return:
        """
        r = requests.get(
            querystring, headers={"Accept": self.response_content_type},
        )
        if not r.ok:
            r.raise_for_status()
        return r.content

    @staticmethod
    def query_protein_to_paperblast(seq):

        opener = urllib.request.build_opener()
        try:
            response = opener.open(seq, timeout=60)
            sleep(1)
            logger.info("Successful query_protein_to_paperblast.")
            return response
        except Exception as ex:
            logger.info(f"query_protein_to_paperblast Error: {ex}.")
            return None

    def get_genes_from_accession_code(self, accession_code: str) -> list:

        genes = accession_code.split(" / ")
        return genes

    def parse_protein_information(self, protein_tag) -> dict:
        """

        :param protein_tag:
        :return:
        """

        protein_information = {}
        list_of_variable_names = [
            "functional_role",
            "accession_code",
            "article_list",
            "database_name",
            "genome_name",
            "genes",
        ]
        (
            functional_role,
            accession_code,
            article_list,
            database_name,
            genome_name,
            genes,
        ) = ("", "", [], "", "", [])

        for child in protein_tag.children:
            if child.name:
                if child.name == "br":
                    # load function variables into dict
                    loc = locals()
                    function = {
                        name: loc[name] for name in list_of_variable_names
                    }
                    # save function dict into protein_information using accession_code if db is relevant
                    if (
                        accession_code
                        and article_list
                        and database_name in self._targeted_sites
                    ):
                        protein_information[accession_code] = function

                # parse database_entries, accession_code, genes
                if child.has_attr("title") and child.has_attr("onmousedown"):
                    database_name = str(child["title"])
                    accession_code = child.contents[0]
                    genes = self.get_genes_from_accession_code(accession_code)

                # parse functional_role
                if child.name == "b":
                    functional_role = str(child.contents[0])
                    # functional_role = str(child.contents[0]).encode('utf-8')

                # parse genome_name
                if child.name == "i":
                    genome_name = str(child.string)

                # parse Pubmed entries
                if child.has_attr("onmousedown"):
                    pubmed_url = child["href"]
                    if pubmed_url and accession_code:
                        article_list = pubmed_url.lstrip(
                            "http://www.ncbi.nlm.nih.gov/pubmed/",
                        ).split(",")
                        article_list = [
                            str(elt) for elt in article_list if elt.isdigit()
                        ]

        # load function variables into dict
        loc = locals()
        function = {name: loc[name] for name in list_of_variable_names}
        # save function dict into protein_information using accession_code
        if (
            accession_code
            and article_list
            and database_name in self._targeted_sites
        ):
            protein_information[accession_code] = function
        return protein_information

    @staticmethod
    def instantiate_gsp_object(protein_information) -> ProteinEncodingGene:
        """

        :param protein_information:
        :return:
        """

        function_by_protein = {}
        for function_id, function in protein_information.items():
            article_list = [
                Article(pmid=elt) for elt in function["article_list"]
            ]
            database = Database(name=function["database_name"])

            function_obj = Function(
                functional_role=function["functional_role"],
                article_list=article_list,
                protein_encoding_gene_id=function["genes"],
                db_id=database,
                genome_name=function["genome_name"],
            )

            function_by_protein[function_id] = function_obj
        yield ProteinEncodingGene(
            id=None, synonyms=None, function_list=function_by_protein,
        )

    # protein_ids = list(
    #     {
    #         protein_id
    #         for site in database_sources
    #         for protein_id in database_sources[site]
    #         if protein_id[0].isupper()
    #     },
    # )

    # if protein_ids:
    #     # For all protein_id founds in protein_ids, an object is instantiated with all features collected above
    #     for protein_id in protein_ids:
    #         if protein_id not in curated_entry_evidences:
    #             if entry_evidence["genome_names"]:
    #                 matches_pubseed_features = []
    #
    #                 gsp = GoldStandardProtein(
    #                     accession_code_list=protein_ids,
    #                     database_sources=database_sources,
    #                     entry_id=protein_id,
    #                     functional_roles=databases_functional_role,
    #                     genome_names=entry_evidence[
    #                         "genome_names"
    #                     ],
    #                     pubseed_identifiers=matches_pubseed_features,
    #                     dbs_associated_pubmed_identifiers=databases_pubmed_ids,
    #                     genes=entry_evidence["genes"],
    #                 )
    #
    #                 curated_entry_evidences[protein_id] = gsp

    @log_method
    @ratelimiter.RateLimiter(max_calls=100, period=3600)  # 100 hits per hour
    @ratelimiter.RateLimiter(max_calls=150, period=60)  # 150 hits per min
    @ratelimiter.RateLimiter(max_calls=5, period=1)  # 5 hits per sec
    def get_homologous_gsp_from_protein(self, seq):
        """
        Query protein sequence to http://papers.genomics.lbl.gov/cgi-bin/litSearch.cgi to collect homologous Gold Standard Proteins of the submitted protein sequence
        :param seq:
        :return:
        """
        querystring = self.encode_paperblast_query(seq)
        requests_content = self.get_response_body(querystring)

        if requests_content:
            soup = bs4.BeautifulSoup(
                requests_content.decode("utf-8"), "html.parser",
            )

            protein_feature_list = soup.find_all(
                "p", attrs={"style": "margin-top: 1em; margin-bottom: 0em;"},
            )

            logger.info(f"{len(protein_feature_list)} homologs found")

            for protein_feature in protein_feature_list:
                protein_index = protein_feature_list.index(protein_feature)
                if protein_index:
                    protein_information = self.parse_protein_information(
                        protein_feature,
                    )
                    if protein_information:
                        yield protein_information

    def get_multithreading_multiple_entry_evidences(
        self, targeted_protein_objects,
    ):
        """

        :param targeted_protein_objects:
        :return:
        """

        # make the Pool of workers
        pool = ThreadPool(self.workers)
        logger.info(
            "{} workers are being used for multithreading".format(
                self.workers,
            ),
        )
        # and return the results

        fig_seq = [
            (pubseed_identifier, prot_obj.protein_sequence)
            for prot_obj in targeted_protein_objects
            for pubseed_identifier in prot_obj.pubseed_identifiers
            if prot_obj.protein_sequence and prot_obj.pubseed_identifiers
        ]

        fig_tuple, seq_tuple = zip(*fig_seq)
        fig_list, seq_list = list(fig_tuple), list(seq_tuple)

        args = zip(seq_list)
        multiple_curated_entry_evidences = pool.starmap(
            self.get_homologous_gsp_from_protein, args,
        )
        # close the pool and wait for the work to finish
        pool.close()
        pool.join()

        # Process multithreading process
        multiple_evidence_sets = {}
        for entry_evidence_set in multiple_curated_entry_evidences:
            if entry_evidence_set:
                multiple_evidence_sets.update(entry_evidence_set)
        return multiple_evidence_sets, fig_list

    def initiate_headless_firefox_browser(self):
        """
        initiate_headless_firefox_browser based on selenium
        :return:
        """

        # path of firefox binary
        firefox_binary = FirefoxBinary(
            "C:/Program Files (x86)/Mozilla Firefox/firefox.exe",
        )

        # path of geckodriver
        geckodriver = "C:/Users/lubin.moussu/Documents/SW/geckodriver.exe"
        selenium_browser = SeleniumBrowser(
            firefox_binary=firefox_binary, geckodriver=geckodriver,
        )
        selenium_browser.init_headless_firefox_browser()

        # login to PubSeed
        selenium_browser.init_pubseed_login(
            project_config["seed_identifier"], project_config["seed_pwd"],
        )
        self.selenium_browser = selenium_browser

    def instantiate_pubseed_associated_protein_objects(
        self, targeted_protein_identifiers,
    ):
        # Instantiate Protein object for all targeted_protein_identifiers
        targeted_protein_objects = [
            Protein(protein_id) for protein_id in targeted_protein_identifiers
        ]
        gen = map(
            lambda obj: obj.get_pubseed_entries_for_protein(self.genome_set),
            targeted_protein_objects,
        )

        # Discard protein_obj if it is not assigned to pubseed
        targeted_protein_objects = [
            protein_obj
            for protein_obj in list(gen)
            if protein_obj and protein_obj.pubseed_identifiers
        ]

        fig_list = [
            fig
            for protein_obj in targeted_protein_objects
            for fig in protein_obj.pubseed_identifiers
        ]
        logger.info("{} PubSeed entries found.".format(len(fig_list)))

        # Add pubseed protein information to the protein_object
        targeted_protein_objects = list(
            map(
                lambda obj: obj.set_protein_obj_pubseed_fasta_information(),
                targeted_protein_objects,
            ),
        )

        return targeted_protein_objects

    def multiple_gsp_object_pipeline(self, curated_entry_evidences):
        """
        initiate mltithreading on a set of proteins for which we
        (i) find homologous proteins linked to scientific articles
        (ii) find those homologous proteins in the Seed database
        (iii) assign the scientific article identifiers to the protein in the Seed database
        (iv) check consistency of the annotation
        (v) assign new annotation to the protein if consistency not respected
        :param curated_entry_evidences:
        :return:
        """

        # initiate selenium_browser and login to PubSeed database
        self.initiate_headless_firefox_browser()

        # make the Pool of workers
        pool = ThreadPool(self.workers)
        logger.info(
            "{} workers are being used for multithreading".format(
                self.workers,
            ),
        )
        # and return the results

        # iter multithreading on gsp_object
        gsp_object_list = [
            gsp_object for gsp_object in curated_entry_evidences.values()
        ]

        args = zip(
            gsp_object_list,
            itertools.repeat(self.selenium_browser),
            itertools.repeat(self.ec_number_sufficiency),
            itertools.repeat(self.genome_set),
        )

        # Call gsp_object_pipeline function on each gsp_object
        pool.starmap(GoldStandardProtein.gsp_object_pipeline, args)
        # close the pool and wait for the work to finish
        pool.close()
        pool.join()

        # Quit selenium_browser
        self.selenium_browser.browser.quit()
        return gsp_object_list

    def retrieve_associated_gsp_and_curate_pubseed(
        self, targeted_protein_objects,
    ):
        (
            curated_entry_evidences,
            fig_list,
        ) = self.get_multithreading_multiple_entry_evidences(
            targeted_protein_objects,
        )
        gsp_object_list = self.multiple_gsp_object_pipeline(
            curated_entry_evidences,
        )
        print("{} gsp objects retrieved".format(len(gsp_object_list)))

        # Keep GSP if (i) associated with Pubmed IDs, PubSeed fig
        gsp_object_list = [
            gsp
            for gsp in gsp_object_list
            if gsp.associated_pubmed_identifiers and gsp.pubseed_identifiers
        ]

        return gsp_object_list, fig_list

    def get_all_gold_standard_proteins_of_protein_family(
        self, targeted_protein_objects,
    ):
        depth_iter = 0
        all_gsp_object_list = []
        already_explored_fig_list = []

        """
        From a list of PubSeed fig list, find of related GSPs.
        From all related GSPs, find associated PubSeed fig list
        Redo
        """

        while depth_iter < self.depth:
            # Get all homologous gsp for each of the targeted_protein_objects
            (
                gsp_object_list,
                fig_list,
            ) = self.retrieve_associated_gsp_and_curate_pubseed(
                targeted_protein_objects,
            )

            # save fig already processed and retrieved GSPs
            already_explored_fig_list.extend(fig_list)
            all_gsp_object_list.extend(gsp_object_list)

            # get new pubseed protein to process with self.retrieve_associated_gsp_and_curate_pubseed
            related_pubseed_fig = [
                fig
                for gsp in gsp_object_list
                for fig in gsp.pubseed_identifiers
            ]
            new_ref_fig_list = [
                fig
                for fig in related_pubseed_fig
                if fig not in already_explored_fig_list
            ]

            # instantiate protein_objects and check if there is any pubseed association (there is)
            targeted_protein_objects = self.instantiate_pubseed_associated_protein_objects(
                new_ref_fig_list,
            )

            depth_iter += 1
        return all_gsp_object_list

    @staticmethod
    def get_pubseed_fig_to_curate(all_gsp_object_list):
        # get formet dict[protein_id] = {fig_to_curate,
        #                               annotation
        #                                       }

        gsp_to_curate_pubseed_dict = {}
        for gsp in all_gsp_object_list:
            gsp_to_curate_pubseed_dict[gsp.protein_ID] = {}
            gsp_to_curate_pubseed_dict[gsp.protein_ID][
                "functional_roles"
            ] = gsp.functional_roles
            gsp_to_curate_pubseed_dict[gsp.protein_ID][
                "features_to_curate"
            ] = gsp.features_to_curate
        return gsp_to_curate_pubseed_dict

    def write_gsps_and_related_pubseed_proteins(self, all_gsp_object_list):
        peg_fig = [
            (prot_obj.protein_id, pubseed_identifier)
            for prot_obj in all_gsp_object_list
            for pubseed_identifier in prot_obj.pubseed_identifiers
            if prot_obj.pubseed_identifiers and prot_obj.protein_id
        ]
        fig_tuple, seq_tuple = zip(*peg_fig)
        peg_list, fig_list = list(fig_tuple), list(seq_tuple)

        gsp_dict = {"Peg": peg_list, "Fig": fig_list}

        ref_df = pd.DataFrame(gsp_dict)
        ref_df.set_index("Peg", inplace=True)
        gsp_file = self.file_dictionary["gsp_file"]
        ref_df.to_csv(gsp_file, sep="\t", encoding="utf-8")
        return ref_df

    def get_gsp_and_pubseed_fig_to_curate(self, targeted_protein_names):
        """
        From a list of protein names, retrieve homologous GSPs, related PubSeed protein entries and those that should be curated
        :param targeted_protein_names:
        :return:
        """
        logger.info(
            "{} protein(s) to be found in the PubSeed database.".format(
                len(targeted_protein_names),
            ),
        )
        targeted_protein_objects = self.instantiate_pubseed_associated_protein_objects(
            targeted_protein_names,
        )
        logger.info(
            "{} protein(s) have been found in PubSeed.".format(
                len(targeted_protein_objects),
            ),
        )

        all_gsp_object_list = self.get_all_gold_standard_proteins_of_protein_family(
            targeted_protein_objects,
        )

        self.write_gsps_and_related_pubseed_proteins(all_gsp_object_list)

        gsp_to_curate_pubseed_dict = self.get_pubseed_fig_to_curate(
            all_gsp_object_list,
        )
        return gsp_to_curate_pubseed_dict
