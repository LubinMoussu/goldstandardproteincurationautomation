import logging
import os
from unittest import TestCase

import gsp_curation_automation
from gsp_curation_automation.settings import project_config

logger = logging.getLogger(__name__)


def get_resource_path(filename):
    return os.path.join(
        os.path.dirname(os.path.realpath(__file__)), "data", filename,
    )


def load_testing_configuration():
    """

    :return:
    """
    for (k, v) in gsp_curation_automation.settings.project_config.items():
        if "Path" in k and v:
            setattr(gsp_curation_automation, k, get_resource_path(v))
        else:
            setattr(gsp_curation_automation, k, v)


load_testing_configuration()


class BaseTest(TestCase):
    @classmethod
    def setUpClass(cls) -> None:
        # Load configuration
        load_testing_configuration()

        # Empty testing directories

        # Handle logs
        logging.getLogger().setLevel(logging.DEBUG)
