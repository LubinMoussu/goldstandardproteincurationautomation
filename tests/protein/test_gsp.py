import logging

from gsp_curation_automation.protein.gsp import GoldStandardProtein
from tests.base import BaseTest


logger = logging.getLogger(__name__)


class TestGoldStandardProteinScraper(BaseTest):
    @classmethod
    def setUpClass(cls) -> None:
        """

        :return:
        """
        super().setUpClass()
        cls.protein = GoldStandardProtein(
            accession_code_list=["P42604"],
            database_sources={
                "SwissProt": ["P42604"],
                "EcoCyc": ["b3091"],
                "MetaCyc": ["P42604"],
                "BRENDA": [],
            },
            entry_id="P42604",
            functional_roles={
                "SwissProt": [
                    "Altronate dehydratase; D-altronate hydro-lyase; EC 4.2.1.7",
                ],
                "EcoCyc": ["D-altronate dehydratase (EC 4.2.1.7)"],
                "MetaCyc": ["D-altronate dehydratase (EC 4.2.1.7)"],
                "BRENDA": [],
            },
            genome_names=[
                "Escherichia coli K-12 substr. MG1655",
                "Escherichia coli (strain K12)",
                "Escherichia coli str. K-12 substr. MG1655",
            ],
            pubseed_identifiers=[
                "3038546",
                "6997263",
                "7026975",
                "19054114",
                "21873635",
                "19429622",
                "6450743",
            ],
            dbs_associated_pubmed_identifiers={
                "SwissProt": ["19429622", "3038546"],
                "EcoCyc": [
                    "19054114",
                    "21873635",
                    "3038546",
                    "6450743",
                    "6997263",
                    "7026975",
                ],
                "MetaCyc": ["6450743", "6997263", "7026975"],
                "BRENDA": [],
            },
            genes=["YgjW", "UXAA"],
        )

    def test_UpdateFunctionalRoleConsistency_IdenticalFunctionalRoles_ReturnTrue(
        self,
    ):
        current_role_ec = ""
        current_role_ec += "d"
