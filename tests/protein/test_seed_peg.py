import logging

from gsp_curation_automation.protein.seed_peg import SeedPEG
from tests.base import BaseTest


logger = logging.getLogger(__name__)


class TestSeedPEG(BaseTest):
    @classmethod
    def setUpClass(cls) -> None:
        """

        :return:
        """
        super().setUpClass()
