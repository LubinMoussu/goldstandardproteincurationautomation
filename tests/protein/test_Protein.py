import logging

from gsp_curation_automation.protein.protein import Protein
from tests.base import BaseTest


logger = logging.getLogger(__name__)


class TestProtein(BaseTest):
    """

    """

    @classmethod
    def setUpClass(cls) -> None:
        """

        :return:
        """
        super().setUpClass()

    def setUp(self):
        """

        :return:
        """
        self.protein = Protein(protein_id="P0AF18")
        self.uniprot_entry = {
            "annotation_information": "P0AF18",
            "seq": "MYALTQGRIFTGHEFLDDHAVVIADGLIKSVCPVAELPPEIEQRSLNGAILSPGFIDVQLNGCGGVQFNDTAEAVSVETLEIMQKANEKSGCTNYLPTLITTSDELMKQGVRVMREYLAKHPNQALGLHLEGPWLNLVKKGTHNPNFVRKPDAALVDFLCENADVITKVTLAPEMVPAEVISKLANAGIVVSAGHSNATLKEAKAGFRAGITFATHLYNAMPYITGREPGLAGAILDEADIYCGIIADGLHVDYANIRNAKRLKGDKLCLVTDATAPAGANIEQFIFAGKTIYYRNGLCVDENGTLSGSSLTMIEGVRNLVEHCGIALDEVLRMATLYPARAIGVEKRLGTLAAGKVANLTAFTPDFKITKTIVNGNEVVTQ",
            "genome_names": ["Escherichia coli K12"],
            "taxonomic_identifier": "316385",
            "genes": ["nagA"],
        }

    def test_GetGenomeIdFromFig_ValidFig_686659(self):
        """

        :return:
        """
        fig = "fig|686659.3.peg.546"
        genome_id = self.protein.get_genome_id_from_fig(fig)
        self.assertEqual(genome_id, "686659.3")

    def test_GetGenomeIdFromFig_UnvalidFig_EmptyString(self):
        """

        :return:
        """
        fig = "fig|686659.3."
        genome_id = self.protein.get_genome_id_from_fig(fig)
        self.assertEqual(genome_id, "")

    def test_GetFigIntrinsicInformation_ValidFig_ExpectedReturn(self):
        fig = "fig|686659.3.peg.546"
        (
            genome_id,
            genome_version,
            peg,
        ) = self.protein.get_fig_intrinsic_information(fig)
        self.assertEqual(genome_id, "686659")
        self.assertEqual(genome_version, "3")
        self.assertEqual(peg, "546")

    def test_GetFigIntrinsicInformation_UnvalidFig_EmptyStringReturn(self):
        fig = "fig|686659.3.peg."
        (
            genome_id,
            genome_version,
            peg,
        ) = self.protein.get_fig_intrinsic_information(fig)
        self.assertEqual(genome_id, "")
        self.assertEqual(genome_version, "")
        self.assertEqual(peg, "")

    def test_GetFigIntrinsicInformation_EmptyFig_EmptyStringReturn(self):
        fig = ""
        (
            genome_id,
            genome_version,
            peg,
        ) = self.protein.get_fig_intrinsic_information(fig)
        self.assertEqual(genome_id, "")
        self.assertEqual(genome_version, "")
        self.assertEqual(peg, "")

    def test_GetPubseedGenomesWithTaxonomicIdentifier_686659_686659(self):
        genome_id = "686659"
        all_pubseed_genomes = ["686659.3", "1114967.3", "979556.1"]
        matching_genomes = self.protein.get_seed_genomes_from_taxonomic_identifier(
            genome_id, all_pubseed_genomes,
        )
        self.assertEqual(matching_genomes, ["686659.3"])

    def test_GetPubseedGenomesWithTaxonomicIdentifier_686659Versioned_686659(
        self,
    ):
        genome_id = "686659.6"
        all_pubseed_genomes = ["686659.3", "1114967.3", "979556.1"]
        matching_genomes = self.protein.get_seed_genomes_from_taxonomic_identifier(
            genome_id, all_pubseed_genomes,
        )
        self.assertEqual(matching_genomes, ["686659.3"])

    def test_GetPubseedGenomesWithTaxonomicIdentifier_686659Versioned_MultipleMatches(
        self,
    ):
        genome_id = "686659.6"
        all_pubseed_genomes = ["686659.3", "1114967.3", "979556.1", "686659.5"]
        matching_genomes = self.protein.get_seed_genomes_from_taxonomic_identifier(
            genome_id, all_pubseed_genomes,
        )
        self.assertEqual(matching_genomes, ["686659.3", "686659.5"])

    def test_GetPubseedGenomesWithTaxonomicIdentifier_Empty_EmptyListReturn(
        self,
    ):
        genome_id = ""
        all_pubseed_genomes = ["686659.3", "1114967.3", "979556.1", "686659.5"]
        matching_genomes = self.protein.get_seed_genomes_from_taxonomic_identifier(
            genome_id, all_pubseed_genomes,
        )
        self.assertEqual(matching_genomes, [])

    def test_GetCandidateFigFromUniprotProtein_UniprotId_RelatedEntries(self):
        genome_id = "316385.7"
        candidate_fig_list = list(
            self.protein.get_crossed_reference_from_uniprot_protein(
                self.uniprot_entry, genome_id,
            ),
        )
        self.assertListEqual(candidate_fig_list, ["fig|316385.7.peg.747"])

    def test_IsCrossedReference_CrossedReference_ReturnTrue(self):
        fig_information = {
            "length": 382,
            "score": 2024.0,
            "evalue": 0.0,
            "identities": ("382/382", "100"),
        }
        returnal = self.protein.is_crossed_reference(fig_information)
        self.assertTrue(returnal)

    def test_IsCrossedReference_NonCrossedReference_ReturnFalse(self):
        fig_information = {
            "length": 167,
            "score": 245.0,
            "evalue": 1e-25,
            "identities": ("54/162", "33"),
        }
        returnal = self.protein.is_crossed_reference(fig_information)
        self.assertFalse(returnal)

    def test_IsCrossedReference_EmptyFigInformationDict_ReturnFalse(self):
        fig_information = {}
        returnal = self.protein.is_crossed_reference(fig_information)
        self.assertFalse(returnal)

    def test_GetCrossedReferenceFromUniprotProtein_P39829UniprotOn316385Genome_Return316385peg3326(
        self,
    ):
        """

        :return:
        """
        uniprot_entry = {
            "annotation_information": "P39829",
            "seq": "MANIEIRQETPTAFYIKVHDTDNVAIIVNDNGLKAGTRFPDGLELIEHIPQGHKVALLDIPANGEIIRYGEVIGYAVRAIPRGSWIDESMVVLPEAPPLHTLPLATKVPEPLPPLEGYTFEGYRNADGSVGTKNLLGITTSVHCVAGVVDYVVKIIERDLLPKYPNVDGVVGLNHLYGCGVAINAPAAVVPIRTIHNISLNPNFGGEVMVIGLGCEKLQPERLLTGTDDVQAIPVESASIVSLQDEKHVGFQSMVEDILQIAERHLQKLNQRQRETCPASELVVGMQCGGSDAFSGVTANPAVGYASDLLVRCGATVMFSEVTEVRDAIHLLTPRAVNEEVGKRLLEEMEWYDNYLNMGKTDRSANPSPGNKKGGLANVVEKALGSIAKSGKSAIVEVLSPGQRPTKRGLIYAATPASDFVCGTQQVASGITVQVFTTGRGTPYGLMAVPVIKMATRTELANRWFDLMDINAGTIATGEETIEEVGWKLFHFILDVASGKKKTFSDQWGLHNQLAVFNPAPVT",
            "genome_names": ["Escherichia coli K12"],
            "taxonomic_identifier": "83333",
            "genes": ["garD"],
        }
        fig_list = list(
            self.protein.get_crossed_reference_from_uniprot_protein(
                uniprot_entry=uniprot_entry, genome_id="316385.7",
            ),
        )
        self.assertListEqual(fig_list, ["fig|316385.7.peg.3326"])

    def test_GetCrossedReferenceFromUniprotProtein_P0AF18UniprotOn316385Genome_Return316385peg747(
        self,
    ):
        """

        :return:
        """
        fig_list = list(
            self.protein.get_crossed_reference_from_uniprot_protein(
                uniprot_entry=self.uniprot_entry, genome_id="316385.7",
            ),
        )
        self.assertListEqual(fig_list, ["fig|316385.7.peg.747"])

    def test_GetCrossedReferenceFromUniprotProtein_P0AF18UniprotOn757424Genome_ReturnEmptyList(
        self,
    ):
        """

        :return:
        """
        fig_list = list(
            self.protein.get_crossed_reference_from_uniprot_protein(
                uniprot_entry=self.uniprot_entry, genome_id="757424.7",
            ),
        )
        self.assertListEqual(fig_list, [])

    def test_GetPubseedEntriesFromUniprotProteinSequences_P0AF18Uniprot_Return316385peg747(
        self,
    ):
        """

        :return:
        """
        all_pubseed_genomes = [
            "686659.3",
            "1114967.3",
            "979556.1",
            "686659.5",
            "316385.7",
        ]
        fig_list = list(
            self.protein.get_pubseed_entries_from_uniprot_protein_sequences(
                self.uniprot_entry, all_pubseed_genomes,
            ),
        )
        self.assertListEqual(fig_list, ["fig|316385.7.peg.747"])

    def test_GetPubseedEntriesFromUniprotProteinSequences_P0AF18UniprotGenomeNotInGenomeList_ReturnEmptyList(
        self,
    ):
        """

        :return:
        """
        all_pubseed_genomes = ["686659.3", "1114967.3", "979556.1", "686659.5"]
        fig_list = list(
            self.protein.get_pubseed_entries_from_uniprot_protein_sequences(
                self.uniprot_entry, all_pubseed_genomes,
            ),
        )
        self.assertListEqual(fig_list, [])

    def test_GetUUniprotNamesTaxonomyFasta_P0AF18_UniprotEntry(self):
        """

        :return:
        """
        expected_uniprot_entry = {
            "annotation_information": "P0AF18",
            "seq": "MYALTQGRIFTGHEFLDDHAVVIADGLIKSVCPVAELPPEIEQRSLNGAILSPGFIDVQLNGCGGVQFNDTAEAVSVETLEIMQKANEKSGCTNYLPTLITTSDELMKQGVRVMREYLAKHPNQALGLHLEGPWLNLVKKGTHNPNFVRKPDAALVDFLCENADVITKVTLAPEMVPAEVISKLANAGIVVSAGHSNATLKEAKAGFRAGITFATHLYNAMPYITGREPGLAGAILDEADIYCGIIADGLHVDYANIRNAKRLKGDKLCLVTDATAPAGANIEQFIFAGKTIYYRNGLCVDENGTLSGSSLTMIEGVRNLVEHCGIALDEVLRMATLYPARAIGVEKRLGTLAAGKVANLTAFTPDFKITKTIVNGNEVVTQ",
            "genome_names": ["Escherichia coli K12"],
            "taxonomic_identifier": "83333",
            "genes": ["nagA"],
        }

        uniprot_entry = self.protein.get_uniprot_names_taxonomy_fasta("P0AF18")
        self.assertDictEqual(uniprot_entry, expected_uniprot_entry)

    def test_GetUUniprotNamesTaxonomyFasta_P39829_UniprotEntry(self):
        """

        :return:
        """
        expected_uniprot_entry = {
            "annotation_information": "P39829",
            "seq": "MANIEIRQETPTAFYIKVHDTDNVAIIVNDNGLKAGTRFPDGLELIEHIPQGHKVALLDIPANGEIIRYGEVIGYAVRAIPRGSWIDESMVVLPEAPPLHTLPLATKVPEPLPPLEGYTFEGYRNADGSVGTKNLLGITTSVHCVAGVVDYVVKIIERDLLPKYPNVDGVVGLNHLYGCGVAINAPAAVVPIRTIHNISLNPNFGGEVMVIGLGCEKLQPERLLTGTDDVQAIPVESASIVSLQDEKHVGFQSMVEDILQIAERHLQKLNQRQRETCPASELVVGMQCGGSDAFSGVTANPAVGYASDLLVRCGATVMFSEVTEVRDAIHLLTPRAVNEEVGKRLLEEMEWYDNYLNMGKTDRSANPSPGNKKGGLANVVEKALGSIAKSGKSAIVEVLSPGQRPTKRGLIYAATPASDFVCGTQQVASGITVQVFTTGRGTPYGLMAVPVIKMATRTELANRWFDLMDINAGTIATGEETIEEVGWKLFHFILDVASGKKKTFSDQWGLHNQLAVFNPAPVT",
            "genome_names": ["Escherichia coli K12"],
            "taxonomic_identifier": "83333",
            "genes": ["garD"],
        }

        uniprot_entry = self.protein.get_uniprot_names_taxonomy_fasta("P39829")
        self.assertDictEqual(uniprot_entry, expected_uniprot_entry)

    def test_GetUUniprotNamesTaxonomyFasta_NonValidUniprotId_Empty(self):
        """

        :return:
        """
        expected_uniprot_entry = {}
        uniprot_entry = self.protein.get_uniprot_names_taxonomy_fasta("P3")
        self.assertDictEqual(uniprot_entry, expected_uniprot_entry)

    def test_GetLatestSeedPegGivenGenomeVersion_FigListWithRedondantPEG_TrimmedFigList(
        self,
    ):
        fig_list = [
            "fig|316385.1.peg.747",
            "fig|457401.3.peg.1845",
            "fig|316385.8.peg.747",
            "fig|457401.3.peg.1840",
        ]
        updated_pubseed_identifiers = self.protein.get_latest_seed_peg_given_genome_version(
            fig_list,
        )
        self.assertListEqual(
            updated_pubseed_identifiers,
            [
                "fig|316385.8.peg.747",
                "fig|457401.3.peg.1845",
                "fig|457401.3.peg.1840",
            ],
        )

    def test_GetLatestSeedPegGivenGenomeVersion_FigListWithDuplicatePEG_TrimmedFigList(
        self,
    ):
        """

        :return:
        """
        fig_list = [
            "fig|316385.1.peg.747",
            "fig|457401.3.peg.1845",
            "fig|316385.8.peg.747",
            "fig|457401.3.peg.1840",
            "fig|316385.1.peg.747",
        ]
        updated_pubseed_identifiers = self.protein.get_latest_seed_peg_given_genome_version(
            fig_list,
        )
        self.assertListEqual(
            updated_pubseed_identifiers,
            [
                "fig|316385.8.peg.747",
                "fig|457401.3.peg.1845",
                "fig|457401.3.peg.1840",
            ],
        )

    def test_GetLatestSeedPegGivenGenomeVersion_EmptyFigList_EmptyFigList(
        self,
    ):
        """

        :return:
        """
        fig_list = []
        updated_pubseed_identifiers = self.protein.get_latest_seed_peg_given_genome_version(
            fig_list,
        )
        self.assertListEqual(updated_pubseed_identifiers, [])

    def test_GetPubseedEntriesForProtein_UniqueSeedPEG_UniquePEG(self):
        """

        :return:
        """
        all_pubseed_genomes = [
            "686659.3",
            "1114967.3",
            "979556.1",
            "686659.5",
            "316385.8",
        ]
        self.protein = Protein(protein_id="fig|316385.8.peg.747")
        self.protein.get_pubseed_entries_for_protein(all_pubseed_genomes)
        self.assertListEqual(
            self.protein.pubseed_identifiers, ["fig|316385.8.peg.747"],
        )

    def test_GetPubseedEntriesForProtein_MultipleSeedPEG_MultiplePEG(self):
        """

        :return:
        """
        all_pubseed_genomes = [
            "686659.3",
            "1114967.3",
            "979556.1",
            "686659.5",
            "316385.8",
        ]
        self.protein = Protein(protein_id="fig|316385.8.peg.747")
        self.protein.accession_code_list = [
            "fig|316385.4.peg.747",
            "fig|457401.3.peg.1845",
            "fig|316385.8.peg.747",
            "fig|457401.3.peg.1840",
            "fig|316385.1.peg.747",
        ]
        self.protein.get_pubseed_entries_for_protein(all_pubseed_genomes)
        self.assertListEqual(
            self.protein.pubseed_identifiers,
            [
                "fig|316385.8.peg.747",
                "fig|457401.3.peg.1845",
                "fig|457401.3.peg.1840",
            ],
        )
