import logging

from gsp_curation_automation.protein.protein_evidence import ProteinEvidence
from tests.base import BaseTest


logger = logging.getLogger(__name__)


class TestProteinEvidence(BaseTest):
    @classmethod
    def setUpClass(cls) -> None:
        """

        :return:
        """
        super().setUpClass()
