import json
import logging
import os
from unittest import mock

import gsp_curation_automation
import tests.base
from gsp_curation_automation.scrapers.uniprot_fetcher import UniprotFetcher

logger = logging.getLogger(__name__)


def mocked_response_body(file_in):
    """

    :param file_in:
    :return:
    """
    file_in = os.path.join(gsp_curation_automation.mockPath, file_in)

    with open(file_in) as f:
        response_body = json.load(f)
    return response_body


class TestUniprotFetcher(tests.base.BaseTest):
    @classmethod
    def setUpClass(cls) -> None:
        """

        :return:
        """
        super().setUpClass()

    def setUp(self) -> None:
        self.uniprot_fetcher = UniprotFetcher(uniprot_id="P39518")

    def test_makeRetrieveProteinEntryUrl_P39518_stringEqual(self):
        expected_url = "https://www.ebi.ac.uk/proteins/api/proteins/P39518"
        url = self.uniprot_fetcher.make_retrieve_protein_entry_url()
        self.assertEqual(expected_url, url)

    def test_getResponseBody_P39518_AccessionEqualP39518(self):
        # TODO mock it
        expected_url = "https://www.ebi.ac.uk/proteins/api/proteins/P39518"
        response_body = self.uniprot_fetcher.get_response_body(
            request_url=expected_url,
        )

        accession = response_body["accession"]
        self.assertEqual("P39518", accession)

    @mock.patch(
        "gsp_curation_automation.scrapers.uniprot_fetcher.UniprotFetcher.get_response_body",
        return_value=mocked_response_body(file_in="P39518_response_body.json"),
    )
    def test_retrieveProteinEntry_P39518_ResponseBodyDictEqual(self, func):
        """

        :return:
        """
        response_body = self.uniprot_fetcher.retrieve_protein_entry()
        expected = mocked_response_body(file_in="P39518_response_body.json")
        self.assertDictEqual(expected, response_body)

    def test_filterProteinEntry_P39518_responseBodyKeySetEqual(self):
        """

        :return:
        """
        response_body = mocked_response_body(
            file_in="P39518_response_body.json",
        )
        filtered_response_body = self.uniprot_fetcher.filter_protein_entry(
            response_body=response_body,
        )
        expected = {"accession", "sequence", "organism", "gene"}
        self.assertSetEqual(expected, set(filtered_response_body.keys()))

    def test_filterProteinEntry_P39518AndEmptykeys_emptyFilteredResponseBody(
        self,
    ):
        """

        :return:
        """
        response_body = mocked_response_body(
            file_in="P39518_response_body.json",
        )
        self.uniprot_fetcher.key2filter = []
        filtered_response_body = self.uniprot_fetcher.filter_protein_entry(
            response_body=response_body,
        )
        self.assertDictEqual({}, filtered_response_body)
