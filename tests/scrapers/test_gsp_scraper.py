import logging
import os

import bs4

import gsp_curation_automation
from gsp_curation_automation.scrapers.gsp_scraper import (
    GoldStandardProteinScraper,
)
from gsp_curation_automation.utils.miscellaneous import file2list
from gsp_curation_automation.utils.miscellaneous import list2file
from tests.base import BaseTest

logger = logging.getLogger(__name__)


def load_mocked_response_body(file_in):
    """

    :param file_in:
    :return:
    """
    file_in = os.path.join(gsp_curation_automation.mockPath, file_in)
    line_list = file2list(filename=file_in)
    return line_list[0]


def save_mocked_response_body(request, file_out):
    request_list = [request]
    file_in = os.path.join(gsp_curation_automation.mockPath, file_out)
    list2file(line_list=request_list, filename=file_in)


class TestGoldStandardProtein(BaseTest):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        depth = 1  # Gold Standard Proteins found with 1 iteration can be used as inputs to find their homologs.
        # depth defines the number of times newly found Gold Standard Proteins are re-used to find new homologs
        workers = 3
        ec_number_sufficiency = True
        cls.gsp_scraper = GoldStandardProteinScraper(
            workers, ec_number_sufficiency, depth,
        )

    def test_getResponseBody_(self):
        # TODO mock it
        querystring = "http://papers.genomics.lbl.gov/cgi-bin/litSearch.cgi?query=VIMSS3615187"
        request = self.gsp_scraper.get_response_body(querystring)
        print(type(request))

        save_mocked_response_body(
            request, file_out="VIMSS3615187_repsonse_body",
        )

    def test_parseProteinInformation_correctTagElement_ProteinInfoDictEqual(
        self,
    ):
        s = """<p style="margin-top: 1em; margin-bottom: 0em;"><a href="http://www.uniprot.org/uniprot/Q58Y43" onmousedown="logger(this, 'curated::SUYB_PARPN / Q58Y43')" title="SwissProt">SUYB_PARPN / Q58Y43</a> <b>(2R)-sulfolactate sulfo-lyase subunit beta; Sulfolactate sulfo-lyase B; EC 4.4.1.24</b> from <i>Paracoccus pantotrophus (Thiosphaera pantotropha)</i> (see <a href="http://www.ncbi.nlm.nih.gov/pubmed/15758220" onmousedown="logger(this, 'curatedpaper::SUYB_PARPN / Q58Y43')">paper</a>)<br/><a href="https://metacyc.org/gene?orgid=META&amp;id=MONOMER-15874" onmousedown="logger(this, 'curated::suyB / Q58Y43')" title="MetaCyc">suyB / Q58Y43</a> <b>3-sulfolactate sulfo-lyase large subunit (EC 4.4.1.24)</b> from <i>Paracoccus pantotrophus</i> (see <a href="http://www.ncbi.nlm.nih.gov/pubmed/15758220" onmousedown="logger(this, 'curatedpaper::suyB / Q58Y43')">paper</a>)<br/><a href="showAlign.cgi?def1=b3128 garD (D)-galactarate dehydrogenase (NCBI) (Escherichia coli BW25113)&amp;def2=SUYB_PARPN / Q58Y43&amp;seq1=MANIEIRQETPTAFYIKVHDTDNVAIIVNDNGLKAGTRFPDGLELIEHIPQGHKVALLDIPANGEIIRYGEVIGYAVRAIPRGSWIDESMVVLPEAPPLHTLPLATKVPEPLPPLEGYTFEGYRNADGSVGTKNLLGITTSVHCVAGVVDYVVKIIERDLLPKYPNVDGVVGLNHLYGCGVAINAPAAVVPIRTIHNISLNPNFGGEVMVIGLGCEKLQPERLLTGTDDVQAIPVESASIVSLQDEKHVGFQSMVEDILQIAERHLQKLNQRQRETCPASELVVGMQCGGSDAFSGVTANPAVGYASDLLVRCGATVMFSEVTEVRDAIHLLTPRAVNEEVGKRLLEEMEWYDNYLNMGKTDRSANPSPGNKKGGLANVVEKALGSIAKSGKSAIVEVLSPGQRPTKRGLIYAATPASDFVCGTQQVASGITVQVFTTGRGTPYGLMAVPVIKMATRTELANRWFDLMDINAGTIATGEETIEEVGWKLFHFILDVASGKKKTFSDQWGLHNQLAVFNPAPVT&amp;acc2=SwissProt::Q58Y43" style="font-family: sans-serif; font-size: smaller;" title="119:500/523 of query is similar to 9:372/393 of hit (E = 1e-27,  125 bits)">28% identity, 73% coverage</a><ul style="margin-top: 0em; margin-bottom: 0em;"><li 6em;="" margin-left:="" style="list-style-type: none;"><b>function:</b> Together with SuyA, desulfonates sulfolactate to pyruvate and sulfite. <br/><b>catalytic</b> <b>activity:</b> (R)-3-sulfolactate = H(+) + pyruvate + sulfite <small>(<a href="https://www.rhea-db.org/reaction?id=21428">RHEA:21428</a>)</small><br/><b>subunit:</b> (2R)-sulfolactate sulfo-lyase is composed of a SuyA and a SuyB subunit. </li></ul></p>"""

        root_tag = bs4.BeautifulSoup(s, "html.parser")
        tag = next(root_tag.children)
        protein_information = self.gsp_scraper.parse_protein_information(tag)

        expected_protein_info = {
            "SUYB_PARPN / Q58Y43": {
                "functional_role": "(2R)-sulfolactate sulfo-lyase subunit beta; Sulfolactate sulfo-lyase B; EC 4.4.1.24",
                "accession_code": "SUYB_PARPN / Q58Y43",
                "article_list": ["15758220"],
                "database_name": "SwissProt",
                "genome_name": "Paracoccus pantotrophus (Thiosphaera pantotropha)",
                "genes": ["SUYB_PARPN", "Q58Y43"],
            },
            "suyB / Q58Y43": {
                "functional_role": "3-sulfolactate sulfo-lyase large subunit (EC 4.4.1.24)",
                "accession_code": "suyB / Q58Y43",
                "article_list": ["15758220"],
                "database_name": "MetaCyc",
                "genome_name": "Paracoccus pantotrophus",
                "genes": ["suyB", "Q58Y43"],
            },
        }
        self.assertDictEqual(expected_protein_info, protein_information)

    def test_parseProteinInformation_tagElementWithNoRelevantDB_emptyProteinInfomration(
        self,
    ):
        s = """<p style="margin-top: 1em; margin-bottom: 0em;"><a href="http://www.microbesonline.org/cgi-bin/fetchLocus.cgi?locus=150544" onmousedown="logger(this, 'curated::STM3250')" title="MicrobesOnline">STM3250</a> galactarate dehydrogenase (NCBI ptt file) from <i>Salmonella typhimurium LT2</i><br/><a href="showAlign.cgi?def1=b3128 garD (D)-galactarate dehydrogenase (NCBI) (Escherichia coli BW25113)&amp;def2=STM3250&amp;seq1=MANIEIRQETPTAFYIKVHDTDNVAIIVNDNGLKAGTRFPDGLELIEHIPQGHKVALLDIPANGEIIRYGEVIGYAVRAIPRGSWIDESMVVLPEAPPLHTLPLATKVPEPLPPLEGYTFEGYRNADGSVGTKNLLGITTSVHCVAGVVDYVVKIIERDLLPKYPNVDGVVGLNHLYGCGVAINAPAAVVPIRTIHNISLNPNFGGEVMVIGLGCEKLQPERLLTGTDDVQAIPVESASIVSLQDEKHVGFQSMVEDILQIAERHLQKLNQRQRETCPASELVVGMQCGGSDAFSGVTANPAVGYASDLLVRCGATVMFSEVTEVRDAIHLLTPRAVNEEVGKRLLEEMEWYDNYLNMGKTDRSANPSPGNKKGGLANVVEKALGSIAKSGKSAIVEVLSPGQRPTKRGLIYAATPASDFVCGTQQVASGITVQVFTTGRGTPYGLMAVPVIKMATRTELANRWFDLMDINAGTIATGEETIEEVGWKLFHFILDVASGKKKTFSDQWGLHNQLAVFNPAPVT&amp;acc2=VIMSS150544" style="font-family: sans-serif; font-size: smaller;" title="1:523/523 of query is similar to 1:523/523 of hit (E = 0.0, 1016 bits)">94% identity, 100% coverage</a><ul style="margin-top: 0em; margin-bottom: 0em;"><li 6em;="" margin-left:="" style="list-style-type: none;"><a href="http://www.ncbi.nlm.nih.gov/pmc/articles/PMC4810482" onmousedown="logger(this, 'pb::STM3250')">Genomic Analysis of Salmonella enterica Serovar Typhimurium Characterizes Strain Diversity for Recent U.S. Salmonellosis Cases and Identifies Mutations Linked to Loss of Fitness under Nitrosative and Oxidative Stress</a><br><small><a title="Hayden HS, Matamouros S, Hager KR, Brittnacher MJ, Rohmer L, Radey MC, Weiss EJ, Kim KB, Jacobs MA, Sims-Day EH, Yue M, Zaidi MB, Schifferli DM, Manning SD, Walson JL, Miller SI.">Hayden,</a> mBio 2016 </small><ul style="margin-top: 0em; margin-bottom: 0em;"><li 6em;="" margin-left:="" style="list-style-type: none;">“...these strains: (i) major facilitator superfamily (MFS) xanthosine permease, xapB (STM2421); (ii) galactarate dehydratase, garD (<b><span style="color: red;">STM3250</span></b>); and (iii) a methyl-accepting chemotaxis protein (STM4210). Strains in clade 3 had no uniquely present genes compared to all other strains. When strain SARA9, assigned to a separate lineage (BAPS...”</li></ul></br></li></ul></p>"""
        root_tag = bs4.BeautifulSoup(s, "html.parser")
        tag = next(root_tag.children)
        protein_information = self.gsp_scraper.parse_protein_information(tag)
        self.assertDictEqual({}, protein_information)

    def test_instantiateGspObject_validProteinInformation_objectExists(self):

        protein_information = {
            "suyB / Q58Y43": {
                "functional_role": "3-sulfolactate sulfo-lyase large subunit (EC 4.4.1.24)",
                "accession_code": "suyB / Q58Y43",
                "article_list": ["15758220"],
                "database_name": "MetaCyc",
                "genome_name": "Paracoccus pantotrophus",
                "genes": ["suyB", "Q58Y43"],
            },
        }
        protein_obj = next(
            self.gsp_scraper.instantiate_gsp_object(
                protein_information=protein_information,
            ),
        )
        print(protein_obj.__dict__)

    def test_GetHomologousGspFromProtein_garD_(self):
        curated_homologous_information = self.gsp_scraper.get_homologous_gsp_from_protein(
            "garD",
        )
        curated_homologous_information = [
            elt for elt in curated_homologous_information
        ]
        print(len(curated_homologous_information))

        # protein_ids = list(curated_homologous_information.keys())
        # self.assertListEqual(protein_ids, ["P42604", "Q1QWP0", "Q58Y43"])
        # expected_protein_info = {
        #     "protein_id": "P42604",
        #     "pubseed_identifiers": [],
        #     "accession_code_list": ["P42604"],
        #     "protein_sequence": "",
        #     "genes": ["YgjW", "UXAA"],
        #     "taxonomic_identifier": "",
        #     "function": "",
        #     "pubseed_function": "",
        #     "molecular_function_ontology": [],
        #     "accession_code": ["P42604"],
        #     "database_sources": {
        #         "SwissProt": ["P42604"],
        #         "EcoCyc": ["b3091"],
        #         "MetaCyc": ["P42604"],
        #         "BRENDA": [],
        #     },
        #     "entry_id": "P42604",
        #     "functional_roles": {
        #         "SwissProt": [
        #             "Altronate dehydratase; D-altronate hydro-lyase; EC 4.2.1.7",
        #         ],
        #         "EcoCyc": ["D-altronate dehydratase (EC 4.2.1.7)"],
        #         "MetaCyc": ["D-altronate dehydratase (EC 4.2.1.7)"],
        #         "BRENDA": [],
        #     },
        #     "genome_names": [
        #         "Escherichia coli K-12 substr. MG1655",
        #         "Escherichia coli (strain K12)",
        #         "Escherichia coli (strain K12)",
        #         "Escherichia coli str. K-12 substr. MG1655",
        #     ],
        #     "dbs_associated_pubmed_identifiers": {
        #         "SwissProt": ["19429622", "3038546"],
        #         "EcoCyc": [
        #             "19054114",
        #             "21873635",
        #             "3038546",
        #             "6450743",
        #             "6997263",
        #             "7026975",
        #         ],
        #         "MetaCyc": ["6450743", "6997263", "7026975"],
        #         "BRENDA": [],
        #     },
        #     "associated_pubmed_identifiers": [
        #         "3038546",
        #         "6997263",
        #         "7026975",
        #         "19054114",
        #         "21873635",
        #         "19429622",
        #         "6450743",
        #     ],
        #     "consistency": False,
        #     "features_to_curate": [],
        # }
        # self.assertDictEqual(
        #     curated_entry_evidences["P42604"].__dict__, expected_protein_info,
        # )
