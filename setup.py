#!/usr/bin/env python

try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

with open("README.md") as readme_file:
    readme = readme_file.read()

requirements = [
    "numpy==1.19.1",
    "pandas==1.1.0",
    "python-dateutil==2.8.1",
    "pytz==2020.1",
    "scipy==1.5.2",
    "six==1.15.0",
]

test_requirements = [
    "numpy==1.19.1",
    "pandas==1.1.0",
    "python-dateutil==2.8.1",
    "pytz==2020.1",
    "scipy==1.5.2",
    "six==1.15.0",
]

setup(
    name="GoldStandardProteinCurationAutomation",
    version="0.0.1",
    description="Scraper of GSP and associated papers",
    author="Lubin Moussu",
    author_email="lubin.moussu@gmail.com",
    include_package_data=True,
    install_requires=requirements,
    zip_safe=False,
    classifiers=["Development Status :: 2 - Pre-Alpha"],
    test_suite="tests",
    tests_require=test_requirements,
)
